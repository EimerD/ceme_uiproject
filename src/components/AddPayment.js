import React, {Component} from 'react';
import PaymentRestService from "../services/PaymentRestService";
import {Redirect} from "react-router-dom";
import "../index.css"

export default class AddPayment extends Component {

    constructor(props) {
        super(props);
        this.onChangeId= this.onChangeId.bind(this);
        this.onChangePaymentDate = this.onChangePaymentDate.bind(this);
        this.onChangePaymentType = this.onChangePaymentType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeCustomerId = this.onChangeCustomerId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            id: 0,
            paymentDate: "",
            paymentType: "",
            amount:0,
            customerId:0,
            submitted: false
        };
    }

    onChangeId(e) {
        this.setState({
            id: e.target.value
        });

    }

    onChangePaymentDate(e) {
        this.setState({
            paymentDate: e.target.value
        });
    }

    onChangePaymentType(e) {
        this.setState({
            paymentType: e.target.value
        });
    }


    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    onChangeCustomerId(e) {
        this.setState({
            customerId: e.target.value
        });
    }

    savePayment() {
        var data = {
            id: this.state.id,
            paymentDate: this.state.paymentDate,
            paymentType: this.state.paymentType,
            amount: this.state.amount,
            customerId: this.state.customerId        }

        // {"id":1,"paymentDate":"2020-09-12","paymentType":"cash","amount":0,"customerId":"123"}
        PaymentRestService.create(data)
            .then(response => {
                this.setState({
                    submitted: true
                });

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        if (this.state.submitted) {

            return <Redirect to="/ListPayments" />
        }

        return (<form>
            <h1>Add Payment</h1>
            <div className="form-group">
                <label htmlFor="exampleId">Add payment Id</label>
                <input type="text" className="form-control" id="exampleId" aria-describedby="idHelp"
                       placeholder="Enter Id" value={this.state.id} onChange={this.onChangeId}/>
                <small id="idHelp" className="form-text text-muted">We'll never share your details with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="examplePaymentDate">Payment Date</label>
                <input type="text" className="form-control" id="examplePaymentDate" aria-describedby="paymentDateHelp"
                       placeholder="Enter Payment Date YYYY-MM-DD" value={this.state.paymentDate} onChange=
                           {this.onChangePaymentDate}/>
            </div>
            <div className="form-group">
                <label htmlFor="examplePaymentType">Payment Type</label>
                <input type="text" className="form-control" id="examplePaymentType" aria-describedby="paymentTypeHelp"
                       placeholder="Enter payment type" value={this.state.paymentType} onChange=
                           {this.onChangePaymentType}/>
                <small id="paymentTypeHelp" className="form-text text-muted">We'll never share your payment details with
                    anyone else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleAmount">Payment Amount</label>
                <input type="number" className="form-control" id="exampleAmount" aria-describedby="amountHelp"
                       placeholder="Enter payment amount" value={this.state.amount} onChange={this.onChangeAmount}/>
            </div>
            <div className="form-group">
                <label htmlFor="exampleCustomerId">Customer Id</label>
                <input type="number" className="form-control" id="exampleCustomerId" aria-describedby="CustomerId"
                       placeholder="Enter Customer Id" value={this.state.customerId} onChange={this.onChangeCustomerId}/>
            </div>
            <div className="form-group">

                <input type="submit" className="form-control"  onClick={this.savePayment}/>
            </div>

        </form>)
    }
}
