import React, {Component} from 'react';
import paymentList from "../redux/actions/PaymentListActions";
import connect from "react-redux/lib/connect/connect";
import Accordion from "../components/Accordion";

class  ListPaymentRedux extends  Component {

    constructor(props) {
        super(props);
        console.log("before" + this.props)
        this.props.dispatch(paymentList())
        console.log("after" + this.props)

    }

    render() {
        const {pay, pending, error} = this.props;
        return (
            <div className="col-md-6">
                <h4>Payment List Redux</h4>
                <br/><br/>

                <p>Select the Payment ID to see more information</p>
                <br/><br/>

                <ul className="list-group">
                    {pay &&
                    pay.map((payment, index) => (
                        <li
                            className={
                                "list-group-item  list-group-action" +
                                (index === 1 ?  "active" : "")
                            }
                            // onClick={(prevProps, prevState) => this.getSnapshotBeforeUpdate(prevProps, prevState)}
                            key={index}
                        >
                            <div>
                                <Accordion title={payment.id} content={" Payment date: "
                                    + payment.paymentDate + ", Payment Type: " + payment.paymentType + ", Payment amount $" +
                                    payment.amount + ", Customer Id: " + payment.customerId}/>

                            </div>

                        </li>
                    ))}

                </ul>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};
const mapStateToProps = state => ({
    error:state.error,
    pay: state.pay,
    pending: state.pending
})


export default connect(mapStateToProps,mapDispatchToProps)(ListPaymentRedux);
