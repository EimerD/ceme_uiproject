import React, {Component} from 'react';
import "../index.css"

export default class About extends Component
{
    constructor(props){
        super(props);
        this.state ={ name:"Payment System Service", apistatus:"Unknown"}
    }

    render() {
        return(<div>

            <h1>Connecting to {this.state.name}</h1>
              <p>
                  <br/>
                  <h4> Status is {this.state.apistatus}</h4>

              </p>

        </div>)

    }
    async componentDidMount() {
        var url="http://localhost:8080/payments/status"
        let textData
        try {
            let response = await fetch(url);
            textData = await response.text();
            console.log("The data is: " + textData)

        }
        catch(e)
        {
            console.log('the err is: ' + e.toString())
        }
        this.setState({apistatus: textData})

    }

}
