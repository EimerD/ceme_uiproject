import React, {Component} from 'react';
import "../index.css"

export default class Home extends Component{

    constructor(props){
        super(props);
        this.state ={ name:"Payment System Service", apicount:"unknown"}

    }

    render() {
        return(<div>
            <h1><i> Welcome to the Payment Service Home page</i> </h1>

            <br/>
            <h4> Number of entries in the Payment service is <u>
                {this.state.apicount} </u></h4>

            <br/><br/>
            <div class="text-center">
                <img src="/images/mobile.jpg" className="rounded f" alt=""/>
            </div>


        </div>)

    }
    async componentDidMount() {
        var url="http://localhost:8080/payments/count"
        let textData
        try {
            let response = await fetch(url);
            textData = await response.text();
            console.log("The data is: " + textData)

            }
            catch(e)
            {
                console.log('the err is: ' + e.toString())
            }
            this.setState({apicount: textData})

     }



}

// function Home(props){
//     return(
//         <div>
//             <h1>Home Page</h1>
//             <h2>{props.appName}</h2>
//         </div>
//     )
// }
// export default Home;