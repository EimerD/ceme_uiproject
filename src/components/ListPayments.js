import React, {Component} from 'react';
import PaymentRestService from "../services/PaymentRestService";
import "../index.css"

export default class  ListPayments extends  Component {
// function ListPayments(props) {
//     return (<div>
//
//         <h1>List Payment Page</h1>
//         <h2>{props.appname}</h2>
//     </div>)
        constructor(props) {
            super(props);
            this.state = {
                pay: [],
                currentIndex: -1
            }
        }

        retrievePayments() {
            PaymentRestService.getAll()
                .then(response => {
                    this.setState({
                        pay: response.data
                    });
                    console.log(response.data);

                })
                .catch(e => {
                    console.log(e);
                });
        }

        componentDidMount() {
            this.retrievePayments();
        }

        render() {
            const {pay, currentIndex} = this.state;
            return (
                <div className="col-md-6">
                    <h1>Payment List</h1>

                <br/> <br/>
                    <table className="table">
                        <thead className="thead-dark" border ="1" width = "80%">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Payment Date</th>
                            <th scope="col">Payment Type</th>
                            <th scope="col">Payment Amount</th>
                            <th scope="col">Customer ID</th>
                        </tr>
                        </thead>
                        <tbody>
                        {pay &&
                        pay.map((payment, index) => (
                        <tr  className={
                            "table-item " +
                            (index === currentIndex ? "active" : "")
                        }
                             key={index}>
                            <td>{payment.id} </td>
                            <td>{payment.paymentDate} </td>
                            <td>{payment.paymentType} </td>
                            <td>{payment.amount} </td>
                            <td>{payment.customerId}</td>
                        </tr>
                        ))}
                        </tbody>

                    </table>
                </div>
            )
        }
    }