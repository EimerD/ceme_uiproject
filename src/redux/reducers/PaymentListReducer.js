import {PAYMENT_LIST_REQUEST , PAYMENT_LIST_SUCCESS , PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"

const paymentsInitialState = {
    pending: false,
    pay: [],
    error: null
}

function PaymentListReducer(state=paymentsInitialState,action)
{
    console.log("Reducer Type = " + action.type);

    switch (action.type) {
        case PAYMENT_LIST_REQUEST:
            console.log("Reducer 1");
            return { ...state, pending: true };
        case PAYMENT_LIST_SUCCESS:
            console.log(">>> reducer 2");
            return { ...state, pending: false, pay: action.payload };
        case PAYMENT_LIST_FAIL:
            console.log(">>> reducer 3");
            return { ...state, pending: false, error: action.payload };

        default:
            return state;
    }
}
export default PaymentListReducer;