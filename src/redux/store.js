import {applyMiddleware, createStore} from "redux";
import  PaymentListReducer from "./reducers/PaymentListReducer"
import thunk from "redux-thunk";
const initialState = {};
export const middlewares = [thunk];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__

const store = createStore(
    PaymentListReducer,
    initialState,
    composeEnhancer(applyMiddleware(...middlewares))
);
export default store;
