import {PAYMENT_LIST_REQUEST , PAYMENT_LIST_SUCCESS , PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"

import PaymentRestService from "../../services/PaymentRestService";


const paymentList = () =>  (dispatch) => {
    try {
        console.log(">>> in dispatch ");
        dispatch({ type: PAYMENT_LIST_REQUEST });
        const data =  PaymentRestService.getAll().then(response => {
            dispatch({type: PAYMENT_LIST_SUCCESS, payload: response.data });
        });
        console.log(">>> Payment list = + got data");

    } catch (error) {
        console.log(">>> in dispatch error=" + error);
        dispatch({ type: PAYMENT_LIST_FAIL, payload: error });
    }
};
export default paymentList;