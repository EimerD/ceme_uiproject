import http from "../http-common";
export class PaymentRestService {
    getAll(){
        return http.get("/all");
    }
    get(id){
        return http.get(`/find/${id}`);
    }
    gettype(type){
        return http.get(`/find/${type}`);
    }
    create(data){
        return http.post("/save", data);
    }
}
export default new PaymentRestService();