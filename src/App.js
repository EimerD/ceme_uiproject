
import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css"
import AddPayment from "./components/AddPayment";
import ListPayments from "./components/ListPayments";
import About from "./components/About";
import Home from "./components/Home";
import ListPaymentRedux from "./components/ListPaymentRedux";


class App extends Component {
  render() {
    return (
        <div id ="container">
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a href="/Home" className="navbar-brand">
              Payment Service
            </a>
            <div className="navbar-nav mr-auto">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item">
                <Link to={"/ListPayments"} className="nav-link">
                  Payments List
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/add"} className="nav-link">
                  Add Payment
                </Link>
              </li>
                <li className="nav-item">
                  <Link to={"/ListPaymentRedux"} className="nav-link">
                    List Payment Redux
                  </Link>
                </li>

              <li className="nav-item">
                <Link to={"/About"} className="nav-link">
                  About
                </Link>
              </li>

              </ul>

            </div>
            <div>
              <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                  <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
              </form>

            </div>
          </nav>


          <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/ListPayments"]} component={ListPayments}/>
              <Route exact path="/add" component={AddPayment}/>
              <Route path="/Home" component={Home}/>
              <Route path="/ListPaymentRedux" component={ListPaymentRedux}/>
              <Route path="/About" component={About}/>
            </Switch>
          </div>
        </div>


    );
  }
}

export default App;